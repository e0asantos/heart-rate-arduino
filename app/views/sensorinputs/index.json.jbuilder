json.array!(@sensorinputs) do |sensorinput|
  json.extract! sensorinput, :id, :raw_info, :data_type
  json.url sensorinput_url(sensorinput, format: :json)
end
