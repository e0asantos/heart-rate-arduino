require 'pusher'
class SensorinputsController < ApplicationController
  before_action :set_sensorinput, only: [:show, :edit, :update, :destroy]
  protect_from_forgery except: :create

  def sensorin
    responseto="OK"
    Pusher.app_id = '185633'
    Pusher.key = '845298bc064d47d6943d'
    Pusher.secret = 'ffc4142d5ee0ee498159'
    Pusher.logger = Rails.logger
    Pusher.encrypted = true
    if params[:q].index("S")!=nil
      Pusher.trigger('test_channel', 'my_event', {
        message: params[:q][1..3]
      })
    end
    if params[:q].index("B")!=nil
      Pusher.trigger('test_channel', 'my_event', {
        beat: params[:q][1..3]
      })  
    end
    
    respond_to do |format|
        format.json { render json: responseto.to_json }
    end
  end
  # GET /sensorinputs
  # GET /sensorinputs.json
  def index
    @sensorinputs = Sensorinput.all
  end

  # GET /sensorinputs/1
  # GET /sensorinputs/1.json
  def show
  end

  # GET /sensorinputs/new
  def new
    @sensorinput = Sensorinput.new
  end

  # GET /sensorinputs/1/edit
  def edit
  end

  # POST /sensorinputs
  # POST /sensorinputs.json
  def create
    @sensorinput = Sensorinput.new(sensorinput_params)

    respond_to do |format|
      if @sensorinput.save
        format.html { redirect_to @sensorinput, notice: 'Sensorinput was successfully created.' }
        format.json { render :show, status: :created, location: @sensorinput }
      else
        format.html { render :new }
        format.json { render json: @sensorinput.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sensorinputs/1
  # PATCH/PUT /sensorinputs/1.json
  def update
    respond_to do |format|
      if @sensorinput.update(sensorinput_params)
        format.html { redirect_to @sensorinput, notice: 'Sensorinput was successfully updated.' }
        format.json { render :show, status: :ok, location: @sensorinput }
      else
        format.html { render :edit }
        format.json { render json: @sensorinput.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sensorinputs/1
  # DELETE /sensorinputs/1.json
  def destroy
    @sensorinput.destroy
    respond_to do |format|
      format.html { redirect_to sensorinputs_url, notice: 'Sensorinput was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sensorinput
      @sensorinput = Sensorinput.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sensorinput_params
      params.require(:sensorinput).permit(:raw_info, :data_type)
    end
end
