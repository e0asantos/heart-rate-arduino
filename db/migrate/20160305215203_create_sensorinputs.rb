class CreateSensorinputs < ActiveRecord::Migration
  def change
    create_table :sensorinputs do |t|
      t.integer :raw_info
      t.string :data_type

      t.timestamps
    end
  end
end
