require 'test_helper'

class SensorinputsControllerTest < ActionController::TestCase
  setup do
    @sensorinput = sensorinputs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sensorinputs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sensorinput" do
    assert_difference('Sensorinput.count') do
      post :create, sensorinput: { data_type: @sensorinput.data_type, raw_info: @sensorinput.raw_info }
    end

    assert_redirected_to sensorinput_path(assigns(:sensorinput))
  end

  test "should show sensorinput" do
    get :show, id: @sensorinput
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sensorinput
    assert_response :success
  end

  test "should update sensorinput" do
    patch :update, id: @sensorinput, sensorinput: { data_type: @sensorinput.data_type, raw_info: @sensorinput.raw_info }
    assert_redirected_to sensorinput_path(assigns(:sensorinput))
  end

  test "should destroy sensorinput" do
    assert_difference('Sensorinput.count', -1) do
      delete :destroy, id: @sensorinput
    end

    assert_redirected_to sensorinputs_path
  end
end
